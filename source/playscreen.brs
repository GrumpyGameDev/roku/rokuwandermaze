Function PlayScreen_new(parent)
	this = {}
	this.parent = parent
	this.Background = CreateObject("roBitmap","pkg:/images/playscreen/background.png")
	this.Sounds = {
		Gold:CreateObject("roAudioResource","pkg:/audio/Pickup_Coin17.wav"),
		Death:CreateObject("roAudioResource","pkg:/audio/Explosion3.wav"),
		Water:CreateObject("roAudioResource","pkg:/audio/Powerup4.wav"),
		Protected:CreateObject("roAudioResource","pkg:/audio/Laser_Shoot4.wav"),
		Wall:CreateObject("roAudioResource","pkg:/audio/Hit_Hurt10.wav")
	}
	this.Tiles =[
		CreateObject("roBitmap","pkg:/images/playscreen/empty.png"),
		CreateObject("roBitmap","pkg:/images/playscreen/meeple.png"),
		CreateObject("roBitmap","pkg:/images/playscreen/gold-bar.png"),
		CreateObject("roBitmap","pkg:/images/playscreen/rock.png"),
		CreateObject("roBitmap","pkg:/images/playscreen/water-drop.png"),
		CreateObject("roBitmap","pkg:/images/playscreen/fire.png")
	]
	this.Labels={
		Alive:CreateObject("roBitmap","pkg:/images/playscreen/alive.png"),
		Dead:CreateObject("roBitmap","pkg:/images/playscreen/dead.png"),
		GoldLeft:CreateObject("roBitmap","pkg:/images/playscreen/gold-left.png"),
		Level:CreateObject("roBitmap","pkg:/images/playscreen/level.png"),
		Protection:CreateObject("roBitmap","pkg:/images/playscreen/protection.png"),
		Score:CreateObject("roBitmap","pkg:/images/playscreen/score.png"),
		Status:CreateObject("roBitmap","pkg:/images/playscreen/status.png")
	}
	this.Numbers={
		Black:[
			CreateObject("roBitmap","pkg:/images/playscreen/black0.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/black1.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/black2.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/black3.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/black4.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/black5.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/black6.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/black7.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/black8.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/black9.png")
		],
		Pink:[
			CreateObject("roBitmap","pkg:/images/playscreen/pink0.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/pink1.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/pink2.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/pink3.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/pink4.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/pink5.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/pink6.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/pink7.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/pink8.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/pink9.png")
		],
		Blue:[
			CreateObject("roBitmap","pkg:/images/playscreen/blue0.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/blue1.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/blue2.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/blue3.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/blue4.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/blue5.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/blue6.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/blue7.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/blue8.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/blue9.png")
		],
		Yellow:[
			CreateObject("roBitmap","pkg:/images/playscreen/yellow0.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/yellow1.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/yellow2.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/yellow3.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/yellow4.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/yellow5.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/yellow6.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/yellow7.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/yellow8.png"),
			CreateObject("roBitmap","pkg:/images/playscreen/yellow9.png")
		]
	}
	this.Player={
		Level:0,
		X:Rnd(32)-1,
		Y:Rnd(32)-1,
		Score:0,
		GoldLeft:0,
		Protection:0,
		Alive:True
	}
	this.Board=CreateObject("roArray",32,false)
	For x=0 To 31
		this.Board[x]=CreateObject("roArray",32,false)
	End For
	PlayScreen_InitLevel(this)
	return this
End Function

Sub PlayScreen_InitLevel(this)
	For x=0 To 31
		For y=0 To 31
			this.Board[x][y]=0
		End for
	End For
	this.Board[this.Player.X][this.Player.y]=1
'	counts:{gold:64,blocks:32,death:64,protection:9},
'	increments:{gold:64,blocks:8,death:64,protection:-0.25},
'	maximums:{gold:64,blocks:128,death:512,protection:8}
	gold = 64
	this.Player.GoldLeft=gold
	While gold>0
		x=Rnd(32)-1
		y=Rnd(32)-1
		If this.Board[x][y]=0 Then
			this.Board[x][y]=2
			gold=gold-1
		End If
	End While
	blocks = 64 + this.Player.Level * 8
	If blocks>128 Then
		blocks=128
	End If
	While blocks>0
		x=Rnd(32)-1
		y=Rnd(32)-1
		If this.Board[x][y]=0 Then
			this.Board[x][y]=3
			blocks=blocks-1
		End If
	End While
	fire = 64+this.Player.Level * 64
	If fire>512 Then
		fire=512
	End If
	While fire>0
		x=Rnd(32)-1
		y=Rnd(32)-1
		If this.Board[x][y]=0 Then
			this.Board[x][y]=5
			fire=fire-1
		End If
	End While
	water = Int(9-this.Player.Level/4)
	If water<0 Then
		water=0
	End If
	While water>0
		x=Rnd(32)-1
		y=Rnd(32)-1
		If this.Board[x][y]=0 Then
			this.Board[x][y]=4
			water=water-1
		End If
	End While
End Sub

Function PlayScreen_dtor(this)
	this.parent=invalid
End Function

Sub DrawNumberCentered(screen, bitmaps, value, x, y)
	text = value.ToStr()
	Print Len(text)
	x=x-15*Len(text)
	for p = 1 to Len(text)
		v = Val(Mid(text,p,1))
		screen.DrawObject(x,y,bitmaps[v])
		x=x+30
	End For
End Sub

Sub PlayScreen_Draw(this As Object)
	Screen = this.parent.Screen
	Screen.Clear(&h000000FF)
	Screen.DrawObject(0,0,this.Background)
	For x=0 To 31
		For y=0 To 31
			Screen.DrawObject(288+22*x,8+22*y,this.Tiles[this.Board[x][y]])
		End For
	End For
	Screen.DrawObject(0,0,this.Labels.Status)
	If this.Player.Alive Then
		Screen.DrawObject(0,48,this.Labels.Alive)
	Else
		Screen.DrawObject(0,48,this.Labels.Dead)
	End If
	Screen.DrawObject(1000,0,this.Labels.Level)
	DrawNumberCentered(Screen, this.Numbers.Pink, this.Player.Level, 1140, 48)
	Screen.DrawObject(1000,96,this.Labels.Score)
	DrawNumberCentered(Screen, this.Numbers.Black, this.Player.Score, 1140, 144)
	Screen.DrawObject(1000,192,this.Labels.GoldLeft)
	DrawNumberCentered(Screen, this.Numbers.Yellow, this.Player.GoldLeft, 1140, 240)
	Screen.DrawObject(1000,288,this.Labels.Protection)
	DrawNumberCentered(Screen, this.Numbers.Blue, this.Player.Protection, 1140, 288+48)
	Screen.SwapBuffers()
End Sub

Sub PlayScreen_Move(this,deltaX,deltaY)
	If this.Player.Alive Then
		nextX = this.Player.X+deltaX
		nextY = this.Player.Y+deltaY
		If nextX>=0 And nextX<32 And nextY>=0 And nextY<32 Then
			If this.Board[nextX][nextY]<>3 Then
				this.Board[this.Player.X][this.Player.Y]=5
				this.Player.X=nextX
				this.Player.Y=nextY
				current = this.Board[this.Player.X][this.Player.Y]
				If current=2 Then
					this.Sounds.Gold.Trigger(Application_GetVolume())
					this.Player.Score=this.Player.Score+1
					this.Player.GoldLeft=this.Player.GoldLeft-1
					Print(this.Player.GoldLeft)
					If this.Player.GoldLeft=0 Then
						this.Player.Level = this.Player.Level + 1
						PlayScreen_InitLevel(this)
						Return
					End If
				ElseIf current=4 Then
					this.Sounds.Water.Trigger(Application_GetVolume())
					this.Player.Protection=this.Player.Protection+1
				ElseIf current=5 Then
					If this.Player.Protection>0 Then
						this.Sounds.Protected.Trigger(Application_GetVolume())
						this.Player.Protection=this.Player.Protection-1
					Else
						this.Sounds.Death.Trigger(Application_GetVolume())
						this.Player.Alive=false
					End If
				End If
				this.Board[this.Player.X][this.Player.Y]=1
			Else
				this.Sounds.Wall.Trigger(Application_GetVolume())
			End If
		Else
			this.Sounds.Wall.Trigger(Application_GetVolume())
		End If
	End If
End Sub

Function PlayScreen_EventPump(this As Object) As Boolean
	Message = this.parent.Port.WaitMessage(0)
	If Type(Message) = "roUniversalControlEvent" Then
		If Message.GetInt()=0 Then
			Return True
		ElseIf Message.GetInt()=2 Then
			PlayScreen_Move(this,0,-1)
		ElseIf Message.GetInt()=3 Then
			PlayScreen_Move(this,0,1)
		ElseIf Message.GetInt()=4 Then
			PlayScreen_Move(this,-1,0)
		ElseIf Message.GetInt()=5 Then
			PlayScreen_Move(this,1,0)
		End If
	End If
	Return False
End Function