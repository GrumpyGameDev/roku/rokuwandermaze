'	counts:{gold:64,blocks:32,death:64,protection:9},
'	increments:{gold:64,blocks:8,death:64,protection:-0.25},
'	maximums:{gold:64,blocks:128,death:512,protection:8}
Sub Main()
	Application = Application_new()
	Application_TitleScreen(Application)
End Sub

Function Application_new() As Object
	return Application_ctor({})
End Function

Function Application_ctor(this As Object) As Object
	this.Port = CreateObject("roMessagePort")
	this.Screen = CreateObject("roScreen", true, 1280, 720)
	this.Screen.SetMessagePort(this.Port)
	Return this
End Function

Sub Application_TitleScreen(this As Object)
	titleScreen = TitleScreen_new(this)
	Done = false
	While Not Done
		TitleScreen_Draw(titleScreen)
		Done = TitleScreen_EventPump(titleScreen)
	End While
	TitleScreen_dtor(titleScreen)
End Sub

Sub Application_PlayScreen(this As Object)
	PlayScreen = PlayScreen_new(this)
	Done = false
	While Not Done
		PlayScreen_Draw(PlayScreen)
		Done = PlayScreen_EventPump(PlayScreen)
	End While
	PlayScreen_dtor(PlayScreen)
	PlayScreen=invalid
End Sub

Sub Application_OptionsScreen(this As Object)
	OptionsScreen = OptionsScreen_new(this)
	Done = false
	While Not Done
		OptionsScreen_Draw(OptionsScreen)
		Done = OptionsScreen_EventPump(OptionsScreen)
	End While
	OptionsScreen_dtor(OptionsScreen)
	OptionsScreen=invalid
End Sub

Sub Application_StatsScreen(this As Object)
	StatsScreen = StatsScreen_new(this)
	Done = false
	While Not Done
		StatsScreen_Draw(StatsScreen)
		Done = StatsScreen_EventPump(StatsScreen)
	End While
	StatsScreen_dtor(StatsScreen)
	StatsScreen=invalid
End Sub

Sub Application_HelpScreen(this As Object)
	HelpScreen = HelpScreen_new(this)
	Done = false
	While Not Done
		HelpScreen_Draw(HelpScreen)
		Done = HelpScreen_EventPump(HelpScreen)
	End While
	HelpScreen_dtor(HelpScreen)
	HelpScreen=invalid
End Sub

Sub Application_SetVolume(volume as Integer)
	If volume<0 Then
		volume=0
	End If
	If volume>100 Then
		volume=100
	End If
	section = CreateObject("roRegistrySection","Wandermaze_Options")
	section.Write("Volume",StrI(volume))
	section.Flush()
End Sub

Function Application_GetVolume()
	section = CreateObject("roRegistrySection","Wandermaze_Options")
	If section.Exists("Volume") Then
		return Cint(Val(section.Read("Volume")))
	Else
		return 50
	End If
End Function

