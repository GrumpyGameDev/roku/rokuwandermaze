Function TitleScreen_new(parent As Object) As Object
	Instance = {}
	Instance.parent = parent
	Instance.Background = CreateObject("roBitmap","pkg:/images/titlescreen/background.png")
	Instance.CurrentOption = 0
	Instance.MenuItems=[
	'play
	{Active:CreateObject("roBitmap","pkg:/images/titlescreen/playactive.png"),
	Inactive:CreateObject("roBitmap","pkg:/images/titlescreen/play.png")
	},
	'how to play
	{Active:CreateObject("roBitmap","pkg:/images/titlescreen/helpactive.png"),
	Inactive:CreateObject("roBitmap","pkg:/images/titlescreen/help.png")
	},
	'options
	{Active:CreateObject("roBitmap","pkg:/images/titlescreen/statsactive.png"),
	Inactive:CreateObject("roBitmap","pkg:/images/titlescreen/stats.png")
	},
	'about
	{Active:CreateObject("roBitmap","pkg:/images/titlescreen/optionsactive.png"),
	Inactive:CreateObject("roBitmap","pkg:/images/titlescreen/options.png")
	},
	'quit
	{Active:CreateObject("roBitmap","pkg:/images/titlescreen/quitactive.png"),
	Inactive:CreateObject("roBitmap","pkg:/images/titlescreen/quit.png")
	}
	]
	Instance.NextIndex=[1,2,3,4,0]
	Instance.PreviousIndex=[4,0,1,2,3]
	return Instance
End Function

Function TitleScreen_dtor(this)
	this.parent=invalid
End Function

Sub TitleScreen_Draw(this As Object)
	Screen = this.parent.Screen
	Screen.Clear(&h000000FF)
	Screen.DrawObject(0,0,this.Background)
	For Index=0 To 4
		If Index=this.CurrentOption Then
			Screen.DrawObject(466,40+128*Index,this.MenuItems[Index].Active)
		Else
			Screen.DrawObject(466,40+128*Index,this.MenuItems[Index].Inactive)
		End If
	End For
	Screen.SwapBuffers()
End Sub

Function TitleScreen_EventPump(this As Object) As Boolean
	Message = this.parent.Port.WaitMessage(0)
	If Type(Message) = "roUniversalControlEvent" Then
		If Message.GetInt()=2 Then
			this.CurrentOption = this.PreviousIndex[this.CurrentOption]
			Return False
		ElseIf Message.GetInt()=3 Then
			this.CurrentOption = this.NextIndex[this.CurrentOption]
			Return False
		ElseIf Message.GetInt()=6 Then
			If this.CurrentOption=0 Then
				Application_PlayScreen(this.parent)
			ElseIf this.CurrentOption=1 Then
				Application_HelpScreen(this.parent)
			ElseIf this.CurrentOption=2 Then
				Application_StatsScreen(this.parent)
			ElseIf this.CurrentOption=3 Then
				Application_OptionsScreen(this.parent)
			ElseIf this.CurrentOption=4 Then
				Return True
			End If
			Return False
		End If
	End If
	Return False
End Function