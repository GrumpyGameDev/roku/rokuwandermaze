Function OptionsScreen_new(parent)
	this = {}
	this.parent = parent
	this.Background = CreateObject("roBitmap","pkg:/images/optionsscreen/background.png")
	return this
End Function

Function OptionsScreen_dtor(this)
	this.parent=invalid
End Function

Sub OptionsScreen_Draw(this As Object)
	Screen = this.parent.Screen
	Screen.Clear(&h000000FF)
	Screen.DrawObject(0,0,this.Background)
	Screen.SwapBuffers()
End Sub

Function OptionsScreen_EventPump(this As Object) As Boolean
	Message = this.parent.Port.WaitMessage(0)
	If Type(Message) = "roUniversalControlEvent" Then
		If Message.GetInt()=0 Then
			Return True
		End If
	End If
	Return False
End Function